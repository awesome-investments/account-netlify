module.exports = {
  plugins: {
    tailwindcss: {},
    'postcss-flexbugs-fixes': {},
    'postcss-preset-env': {
      stage: 3,
      features: {
        'custom-properties': false,
      },
      autoprefixer: false,
    },
    'postcss-pxtorem':
      process.env.NODE_ENV === 'production'
        ? {
            rootValue: 16,
            propList: ['*'],
            mediaQuery: false,
          }
        : false,
  },
};
