import { FC } from 'react';

export type SVGProps = {
  width?: number;
  height?: number;
  title?: string;
  className?: string;
};

const SVG: FC<SVGProps> = ({ width = 64, height = 64, title, className, children }) => {
  const hidden = !title;
  return (
    <svg
      width={`${width}px`}
      height={`${height}px`}
      viewBox={`0 0 ${width} ${height}`}
      className={className}
      role="img"
      aria-hidden={hidden}
      focusable={!hidden}
      aria-label={title}
    >
      {title && <title>{title}</title>}
      {children}
    </svg>
  );
};

export default SVG;
