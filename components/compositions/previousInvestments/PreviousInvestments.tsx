import React, { useState } from 'react';
import { PreviousInvestment } from '@/api/apiSchema';
import Grid from '@/components/layout/Grid';
import GridSpan from '@/components/layout/GridSpan';
import Dropdown, { DropdownItemType } from '@/components/ui/dropdown/Dropdown';
import { usePaginationContext } from '@/components/ui/Pagination/PaginationContext';
import withPaginationProvider from '@/components/ui/Pagination/withPaginationProvider';
import PreviousInvestmentsTable, { SortByItem } from '@/compositions/previousInvestments/PreviousInvestmentsTable';

const defaultSortBy: SortByItem = {
  label: 'Disposal date (latest to earliest)',
  value: 'disposalDate.desc',
};

const sortByItems: SortByItem[] = [
  defaultSortBy,
  {
    label: 'Disposal date (earliest to latest)',
    value: 'disposalDate.asc',
  },
  {
    label: 'Investments (A-Z)',
    value: 'name.asc',
  },
  {
    label: 'Investments (Z-A)',
    value: 'name.desc',
  },
  {
    label: 'Code (A-Z)',
    value: 'code.asc',
  },
  {
    label: 'Code (Z-A)',
    value: 'code.desc',
  },
];

type PreviousInvestmentsProps = {
  className?: string;
};

const PreviousInvestments = ({ className = '' }: PreviousInvestmentsProps) => {
  const [sortBy, setSortBy] = useState(defaultSortBy);
  const { resetPagination, data } = usePaginationContext();

  const selectHandler = ({ value, label }: DropdownItemType<PreviousInvestment>) => {
    if (value) {
      resetPagination();
      setSortBy({ label, value });
    }
  };

  return (
    <div className={className}>
      <Grid md={12} className="mb-8 md:mb-10 lg:mb-12">
        <GridSpan mdStart={1} md={5}>
          <Dropdown label="Sort by" id="sortPreviousInvestments" items={sortByItems} onSelect={selectHandler} />
        </GridSpan>
      </Grid>
      <PreviousInvestmentsTable sortBy={sortBy} />
    </div>
  );
};

export default withPaginationProvider(PreviousInvestments);
