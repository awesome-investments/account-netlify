import { FC, ReactNode, useContext, useEffect, useRef } from 'react';
import classNames from 'classnames';
import { GlobalContext } from '@/app/AppContext';

type PageTitleProp = {
  children: ReactNode;
  className?: string;
  weight?: 'light' | 'regular' | 'medium';
};

export const headingClassNames = (headingClass: string, weight: string, className?: string) =>
  classNames(
    headingClass,
    { 'font-light': weight === 'light' },
    { 'font-regular': weight === 'regular' },
    { 'font-medium': weight === 'medium' },
    className,
  );

export const PageTitle: FC<PageTitleProp> = ({ children, className, weight = 'medium' }) => {
  const state = useContext(GlobalContext);
  const ref = useRef<HTMLHeadingElement>(null);
  useEffect(() => {
    if (state.routeChanged) {
      ref?.current?.setAttribute('tabindex', '-1');
      ref?.current?.focus();
    }
  }, []);

  return (
    <>
      <p className="text-sm w-full hidden print:block">Awesome Investments</p>
      <h1 className={headingClassNames('text-xl', weight, className)} ref={ref}>
        {children}
      </h1>
    </>
  );
};
