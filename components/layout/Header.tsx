import Container from './Container';

const Header = () => (
  <>
    <header className="bg-white border-b-1 border-grey-300">
      <div className="border-t-4 border-millenniumMint ">
        <div className="mx-auto max-w-container">
          <Container left right className="hidden md:flex flex-row justify-between items-end text-xl my-4">
            Awesome Investments
          </Container>

          <Container left right className="flex md:hidden flex-row justify-between py-3">
            AI
          </Container>
        </div>
      </div>
    </header>
  </>
);

export default Header;
