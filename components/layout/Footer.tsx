import Container from '@/layout/Container';

const Footer = () => (
  <footer>
    <div className="bg-anchorBlue py-6">
      <div className="max-w-container mx-auto">
        <Container left right className="text-white">
          Awesome Investments
        </Container>
      </div>
    </div>
  </footer>
);

export default Footer;
