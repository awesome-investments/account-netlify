import Head from 'next/head';

type MetadataProps = {
  title?: string;
};

const Metadata = ({ title = 'Awesome Investments' }: MetadataProps) => (
  <Head>
    <title>{title}</title>
  </Head>
);

export default Metadata;
