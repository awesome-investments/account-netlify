import Container from '@/layout/Container';
import Metadata from '@/layout/Metadata';
import { PageTitle } from '@/layout/PageTitle';
import PageWrapper from '@/layout/PageWrapper';

type DefaultLayoutProps = {
  title: string;
  h1?: string | JSX.Element;
  children?: any;
};

const DefaultLayout = ({ title, h1, children }: DefaultLayoutProps) => (
  <>
    <Metadata title={title} />
    <PageWrapper>
      <Container left right className="mt-2 md:mt-5 lg:mt-6 mb-4 md:mb-5 lg:mb-6">
        <PageTitle>{h1 || title}</PageTitle>
      </Container>
      {children}
    </PageWrapper>
  </>
);

export default DefaultLayout;
