import classnames from 'classnames';
import { ChangeEvent } from 'react';

export type DropdownItemType<T> = {
  label: string;
  value?: string;
  custom?: T;
  subItems?: DropdownItemType<T>[];
};

export type DropdownProps<T> = {
  id: string;
  label: string;
  value?: any;
  placeholder?: string;
  helpText?: string;
  items: (DropdownItemType<T> | string)[];
  onSelect: (value: DropdownItemType<T>) => void;
  className?: string;
};

const Dropdown = ({ id, label, value, placeholder, helpText, items, onSelect, className = '' }: DropdownProps<any>) => {
  const displayableItems: DropdownItemType<any>[] = items.map((item) =>
    typeof item === 'string' ? { value: item, label: item } : (item as DropdownItemType<any>),
  );

  const handleChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const clickableItems = displayableItems.map((item) => item.subItems || [item]).flatMap((item) => item);

    const source: DropdownItemType<any>[] = placeholder ? [{ label: placeholder }, ...clickableItems] : clickableItems;
    const index = event.target.selectedIndex;
    onSelect(source[index]);
  };

  const dropdownClass = classnames('min-w-full inline-block', className);

  const selectClass = classnames(
    'text-base',
    'box-border p-3 pr-10 w-full appearance-none focus:outline-none',
    'border border-solid border-grey-700 hover:border-anchorBlueAccent focus:border-anchorBlueAccent',
    'focus:bg-anchorBlueAccent focus:bg-opacity-5 ie-bg-opacity-5',
    'bg-arrowDown bg-no-repeat bg-5 bg-right-3 focus:bg-arrowDown-anchorBlueAccent hover:bg-arrowDown-anchorBlueAccent',
  );

  const displayOption = (item: DropdownItemType<any>, groupKey = '') => (
    <option
      key={groupKey ? item.label : `${groupKey}-${item.label}`}
      value={item.value}
      className="leading-2xl text-black font-effra text-base p-3"
    >
      {item.label}
    </option>
  );

  const displayGroupOption = (groupItem: DropdownItemType<any>) => (
    <optgroup key={groupItem.label} label={groupItem.label} className="not-italic">
      {(groupItem.subItems as DropdownItemType<any>[]).map((subItem) => displayOption(subItem, groupItem.label))}
    </optgroup>
  );

  return (
    <div className={dropdownClass}>
      <label htmlFor={id}>
        <span className="block text-sm text-black font-effra leading-small tracking-normal">{label}</span>

        {helpText ? (
          <span className="block text-sm text-grey-700 font-effra leading-small tracking-normal"> {helpText}</span>
        ) : null}
      </label>

      <div className="mt-2 relative group focus:bg-arrowDown">
        <select id={id} defaultValue={value} onChange={handleChange} className={selectClass}>
          {placeholder ? <option>{placeholder}</option> : null}
          {displayableItems.map((item) => (item.subItems ? displayGroupOption(item) : displayOption(item)))}
        </select>
      </div>
    </div>
  );
};

export default Dropdown;
