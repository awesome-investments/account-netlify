import classNames from 'classnames';
import React, { MouseEventHandler } from 'react';
import { useBreakpoint } from '@/hooks/useBreakpoint';
import GoToPageLink from './GoToPageLink';
import { NextPageLink, PreviousPageLink } from './NavigationPageLink';
import { usePaginationContext } from './PaginationContext';
import { buildPaginationItems, LinkType, translateToPageUnit } from './paginationUtils';

type PaginationListProps = {
  currentPage: number;
  pageItems: LinkType[];
  onPageClick: (page: number) => void;
};

export const PaginationList = ({ currentPage, pageItems, onPageClick }: PaginationListProps) => {
  const linkClasses = classNames('text-anchorBlue');

  const displayPreviousLink = currentPage > 1;
  const displayNextLink = currentPage < pageItems[pageItems.length - 1];

  const pageClickHandler =
    (pageNumber: number): MouseEventHandler<HTMLAnchorElement> =>
    (event) => {
      event.preventDefault();
      onPageClick(pageNumber);
    };

  const previousClickHandler = pageClickHandler(currentPage - 1);
  const nextClickHandler = pageClickHandler(currentPage + 1);

  return (
    <ul className="text-center text-sm md:text-base my-6 -mx-3">
      {displayPreviousLink ? (
        <li className="inline-block pr-2 md:pr-3">
          <PreviousPageLink className={linkClasses} onClickHandler={previousClickHandler} />
        </li>
      ) : null}
      {pageItems.map((item, index) =>
        typeof item === 'number' ? (
          // eslint-disable-next-line react/no-array-index-key
          <li key={index} className="inline-block leading-none p-2 md:p-3">
            <GoToPageLink
              className={linkClasses}
              isSelected={item === currentPage}
              page={item}
              onClickHandler={pageClickHandler(item)}
            />
          </li>
        ) : (
          // eslint-disable-next-line react/no-array-index-key
          <li key={index} className="inline-block">
            <span>...</span>
          </li>
        ),
      )}
      {displayNextLink ? (
        <li className="inline-block pl-2 md:pl-3">
          <NextPageLink className={linkClasses} onClickHandler={nextClickHandler} />
        </li>
      ) : null}
    </ul>
  );
};

const Pagination = () => {
  const breakpoint = useBreakpoint();
  const { pagination, selectPage } = usePaginationContext();
  const { currentPage, totalPages } = translateToPageUnit(pagination);
  const items = buildPaginationItems(currentPage, totalPages, breakpoint);

  return totalPages > 1 ? (
    <nav aria-label="Pagination" className="mb-10">
      <PaginationList pageItems={items} currentPage={currentPage} onPageClick={selectPage} />
    </nav>
  ) : null;
};

export default Pagination;
