import { MouseEventHandler } from 'react';
import ArrowCircleIcon from '@/components/icons/ArrowCircleIcon';
import BaseLink from '../link/BaseLink';

type BasePageLinkProps = {
  className?: string;
  onClickHandler: MouseEventHandler<HTMLAnchorElement>;
};

type NavigationPageLinkProps = BasePageLinkProps & {
  label: string;
  iconClassName?: string;
  title: string;
};

const NavigationPageLink = ({
  className,
  label,
  iconClassName = '',
  title,
  onClickHandler,
}: NavigationPageLinkProps) => {
  const htmlAttrs = { 'aria-label': `${title} page` };
  return (
    <>
      <BaseLink
        link="#"
        className={`${className} hidden md:inline-block`}
        htmlAttrs={htmlAttrs}
        onClick={onClickHandler}
      >
        <span>{label}</span>
      </BaseLink>
      <BaseLink
        link="#"
        className="inline-block md:hidden relative top-2.5"
        htmlAttrs={htmlAttrs}
        onClick={onClickHandler}
      >
        <ArrowCircleIcon width={32} height={32} title={title} className={iconClassName} />
      </BaseLink>
    </>
  );
};

export const PreviousPageLink = ({ className, onClickHandler }: BasePageLinkProps) => (
  <NavigationPageLink
    className={className}
    label="Prev"
    title="Previous"
    iconClassName="transform rotate-180"
    onClickHandler={onClickHandler}
  />
);
export const NextPageLink = ({ className, onClickHandler }: BasePageLinkProps) => (
  <NavigationPageLink className={className} label="Next" title="Next" onClickHandler={onClickHandler} />
);

export default NavigationPageLink;
