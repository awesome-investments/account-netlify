import React, { FC, useEffect, useMemo } from 'react';
import { PaginationData } from '@/api/apiSchema';
import withDataFetcher, { FetchDataResult, UseFetchData } from '@/app/withDataFetcher';
import Pagination from './Pagination';
import { usePaginationContext } from './PaginationContext';

type PaginatedTableProps<DataType> = {
  data: DataType[];
};

type PaginatedTable<DataType> = FC<PaginatedTableProps<DataType>>;

export type PaginationProps = {
  pagination: PaginationData;
};

export type UsePaginatedFetchData<DataFetcherProps, ComponentProps> = (
  fetcherProps: DataFetcherProps & PaginationProps,
) => FetchDataResult<ComponentProps> & PaginationProps;

type UseUpdatePagination = <DataFetcherProps, DataType>(
  useDataFetcher: UsePaginatedFetchData<DataFetcherProps, PaginatedTableProps<DataType>>,
) => UseFetchData<DataFetcherProps, PaginatedTableProps<DataType>>;

const useUpdatePagination: UseUpdatePagination = (useDataFetcher) => (fetcherProps) => {
  const { pagination, setPagination, setData } = usePaginationContext();

  const fetcherResult = useDataFetcher({ ...fetcherProps, pagination });
  const { componentProps } = fetcherResult;

  useEffect(() => {
    if (componentProps) {
      setPagination(fetcherResult.pagination);
      setData(componentProps.data);
    }
  }, [componentProps]);

  return fetcherResult;
};

type WithPaginatedFetcher = <DataFetcherProps, DataType>(
  usePaginatedFetchData: UsePaginatedFetchData<DataFetcherProps, PaginatedTableProps<DataType>>,
  TableComponent: PaginatedTable<DataType>,
) => FC<DataFetcherProps>;

const withPaginatedFetcher: WithPaginatedFetcher = (usePaginatedFetchData, TableComponent) => (props) => {
  const TableWithData = useMemo(() => withDataFetcher(useUpdatePagination(usePaginatedFetchData), TableComponent), []);
  return (
    <>
      <TableWithData {...props} />
      <Pagination />
    </>
  );
};
export default withPaginatedFetcher;
