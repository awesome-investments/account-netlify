import classNames from 'classnames';
import { MouseEventHandler } from 'react';
import BaseLink from '../link/BaseLink';

type PageLinkProps = {
  className?: string;
  isSelected?: boolean;
  page: number;
  onClickHandler: MouseEventHandler<HTMLAnchorElement>;
};

const GoToPageLink = ({ className = '', isSelected = false, page, onClickHandler: onClick }: PageLinkProps) => {
  const pageClasses = classNames(className, { 'rounded-full px-2 py-0.5 text-white bg-black': isSelected });
  const htmlAttrs = { 'aria-label': isSelected ? `Page ${page}, current page` : `Go to page ${page}` };

  return (
    <BaseLink link="#" className={pageClasses} htmlAttrs={htmlAttrs} onClick={onClick}>
      {page}
    </BaseLink>
  );
};

export default GoToPageLink;
