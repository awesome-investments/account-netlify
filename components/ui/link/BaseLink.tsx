import { MouseEventHandler, ReactNode } from 'react';
import Link from 'next/link';

type BaseLinkProps = {
  className?: string;
  link: string;
  children: ReactNode;
  a11ySuffix?: string;
  a11yPrefix?: string;
  htmlAttrs?: { [key: string]: string | number | boolean };
  onClick?: MouseEventHandler<HTMLAnchorElement>;
};

const externalLinkRegex = /^https?:\/\//i;

const A11ySuffix = ({ suffix }: { suffix?: string }) =>
  suffix ? <span className="sr-only">&nbsp;{suffix}</span> : null;

const A11yPrefix = ({ prefix }: { prefix?: string }) =>
  prefix ? <span className="sr-only">{prefix}&nbsp;</span> : null;

const BaseLink = ({ className = '', link, children, htmlAttrs = {}, a11ySuffix, a11yPrefix, onClick }: BaseLinkProps) =>
  externalLinkRegex.test(link) ? (
    <a className={className} href={link} {...htmlAttrs} onClick={onClick}>
      <A11yPrefix prefix={a11yPrefix} />
      {children}
      <A11ySuffix suffix={a11ySuffix} />
    </a>
  ) : (
    <Link href={link}>
      {/* eslint-disable-next-line */}
      <a className={className} {...htmlAttrs} onClick={onClick}>
        <A11yPrefix prefix={a11yPrefix} />
        {children}
        <A11ySuffix suffix={a11ySuffix} />
      </a>
    </Link>
  );

export default BaseLink;
