export const DEFAULT_OFFSET = 0;
export const DEFAULT_LIMIT = 20;

export const paginateData = (offsetParam: string, limitParam: string) => (data: any[]) => {
  const offset = parseInt(offsetParam, 10) || DEFAULT_OFFSET;
  const limit = parseInt(limitParam, 10) || DEFAULT_LIMIT;

  const total = data.length;
  const pageData = data.slice(offset, offset + limit);

  return {
    pagination: { offset, limit, total },
    data: pageData,
  };
};
