import { PreviousInvestment } from '../api/apiSchema';
import { byDateAsc, byDateDesc, byStringAsc, byStringDesc } from '../utils/sortFunctions';
import { ClientNoMock } from './utils/clientNo';
import { paginateData } from './utils/pagination';
import previousInvestmentsMock from './_mocks/previousInvestmentsMock.json';

type CompareFn = (a: PreviousInvestment, b: PreviousInvestment) => number;

const defaultCompareFn: CompareFn = (a, b) => byDateDesc(a.disposalDate, b.disposalDate);

const compareFns: Record<string, CompareFn> = {
  'disposalDate.desc': defaultCompareFn,
  'disposalDate.asc': (a, b) => byDateAsc(a.disposalDate, b.disposalDate),
  'name.asc': (a, b) => byStringAsc(a.name, b.name),
  'name.desc': (a, b) => byStringDesc(a.name, b.name),
  'code.asc': (a, b) => byStringAsc(a.sedol, b.sedol),
  'code.desc': (a, b) => byStringDesc(a.sedol, b.sedol),
};

const sortData = (sortBy: string) => (data: any) => data.sort(compareFns[sortBy] || defaultCompareFn);

const readPreviousInvestmentsMock = (clientNo: ClientNoMock) =>
  Promise.resolve(previousInvestmentsMock[clientNo] as PreviousInvestment[]);

export const getPreviousInvestments = (clientNo: ClientNoMock, sortBy: string, offset: string, limit: string) =>
  readPreviousInvestmentsMock(clientNo).then(sortData(sortBy)).then(paginateData(offset, limit));
