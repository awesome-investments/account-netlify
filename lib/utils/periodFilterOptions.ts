import { Period } from '@/api/apiSchema';
import { DropdownItemType } from '@/components/ui/dropdown/Dropdown';

export const taxYearForDate = (date: Date): number => {
  const taxYearBreakpoint = new Date(`${date.getFullYear()}-04-06T00:00:00Z`);
  if (date.getTime() >= taxYearBreakpoint.getTime()) {
    return date.getFullYear() + 1;
  }
  return date.getFullYear();
};

export const taxYears = (numberOfYears: number, startingFromYear: number) =>
  Array.from({ length: numberOfYears }, (_, i) => i).map((i) => startingFromYear - i);

const taxYearsOptions = (numberOfYears: number, startingFromYear: number): DropdownItemType<Period>[] =>
  taxYears(numberOfYears, startingFromYear).map((year) => ({
    label: `tax year ending ${year}`,
    value: `tax year ending ${year}`,
    custom: { period: year, periodUnit: 'taxYear' },
  }));

export const periodFilterOptions = (
  numberOfTaxYearsInFilter: number,
  startingFromDate: Date,
): DropdownItemType<Period>[] => [
  {
    label: 'last 30 days',
    value: 'last 30 days',
    custom: { period: 30, periodUnit: 'day' },
  },
  {
    label: 'last 90 days',
    value: 'last 90 days',
    custom: { period: 90, periodUnit: 'day' },
  },
  {
    label: 'last 12 months',
    value: 'last 12 months',
    custom: { period: 12, periodUnit: 'month' },
  },
  ...taxYearsOptions(numberOfTaxYearsInFilter, taxYearForDate(startingFromDate)),
];
