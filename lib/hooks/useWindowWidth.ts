import { useEffect, useState } from 'react';
import { debounce } from './debounce';

export const useWindowWidth = (): number => {
  const [currentWidth, setCurrentWidth] = useState(typeof window === 'undefined' ? 0 : window.innerWidth);
  useEffect(() => {
    const onResize = debounce(() => setCurrentWidth(window.innerWidth), 200);
    window.addEventListener('resize', onResize);
    return () => window.removeEventListener('resize', onResize);
  }, []);
  return currentWidth;
};
