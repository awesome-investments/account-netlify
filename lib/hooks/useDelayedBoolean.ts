import { useEffect, useState } from 'react';

type Delay = {
  delayTrue: number;
  delayFalse: number;
};

type UseDelayedBooleanHook = (props: { value: boolean; delay: Delay }) => boolean;

export const useDelayedBoolean: UseDelayedBooleanHook = ({ value, delay }) => {
  const [delayedValue, setDelayedValue] = useState(value);
  useEffect(() => {
    const timeout = setTimeout(() => setDelayedValue(value), value ? delay.delayTrue : delay.delayFalse);
    return () => clearTimeout(timeout);
  }, [value]);
  return delayedValue;
};
