import { useEffect, useState } from 'react';

export const useDelayedValue = <ValueType>({ value, delay }: { value: ValueType | null; delay: number }) => {
  const [delayedValue, setDelayedValue] = useState<ValueType | null>(null);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDelayedValue(value);
    }, delay);

    return () => clearTimeout(timeout);
  }, [value]);

  return delayedValue;
};
