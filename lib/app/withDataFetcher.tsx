import { FC } from 'react';
import Spinner, { PageSpinner } from '@/components/ui/Spinner';

type DataFetcherConfig = {
  skipAnnouncerOnFirstRender?: boolean;
};

export type FetchDataResult<ComponentProps> = {
  componentProps: ComponentProps | undefined;
  error: any;
  announcerMessage?: string;
  config?: DataFetcherConfig;
};

export type UseFetchData<DataFetcherProps, ComponentProps> = (
  fetcherProps: DataFetcherProps,
) => FetchDataResult<ComponentProps>;

type WithDataFetcher = <DataFetcherProps, ComponentProps>(
  useFetchData: UseFetchData<DataFetcherProps, ComponentProps>,
  Component: FC<ComponentProps>,
  SpinnerComponent?: JSX.Element,
) => (fetcherProps: DataFetcherProps) => JSX.Element | null;

const withDataFetcher: WithDataFetcher =
  (useFetchData, Component, SpinnerComponent = <Spinner />) =>
  (fetcherProps) => {
    const { componentProps, error } = useFetchData(fetcherProps);

    return error ? null : <>{componentProps ? <Component {...componentProps} /> : SpinnerComponent}</>;
  };

export const withPageDataFetcher: WithDataFetcher = (useFetchData, Component) =>
  withDataFetcher(useFetchData, Component, <PageSpinner />);

export default withDataFetcher;
