import { NextApiResponse } from 'next';

export const output200 = (response: NextApiResponse) => (data: any) => {
  response.writeHead(200, { 'Content-Type': 'application/json' });
  response.write(JSON.stringify(data));
  response.end();
};
