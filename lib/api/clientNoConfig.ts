let clientNo = '1300107';

// TODO only temporary: to be removed when we've got mocks for Identity Provider
const CLIENT_NO_FRAGMENT = 'clientNo=';

export const readClientNoFromUrl = () => {
  const urlSearch = window.location.search;
  const clientNoStartIndex = urlSearch.indexOf(CLIENT_NO_FRAGMENT);
  if (clientNoStartIndex > -1) {
    clientNo = urlSearch.substring(clientNoStartIndex + CLIENT_NO_FRAGMENT.length);
  }
};

export const getClientNo = () => clientNo;
