import useSWR from 'swr';
import { Fetcher, Key, SWRConfiguration, SWRResponse } from 'swr/dist/types';

type UseApi = <Data = any, Error = any>(
  ...args:
    | readonly [Key]
    | readonly [Key, Fetcher<Data> | null]
    | readonly [Key, SWRConfiguration<Data, Error> | undefined]
    | readonly [Key, Fetcher<Data> | null, SWRConfiguration<Data, Error> | undefined]
) => SWRResponse<Data, Error>;

export const useApi: UseApi = (...props) => {
  const response = useSWR(...props);
  return response;
};
