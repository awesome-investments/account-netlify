import Link from 'next/link';
import Container from '@/layout/Container';
import DefaultLayout from '@/layout/DefaultLayout';

const HomePage = () => (
  <DefaultLayout title="Awesome Investments" h1="Awesome Investments">
    <Container left right>
      <Link href="/previousInvestments">
        <a className="underline">Investments</a>
      </Link>
    </Container>
  </DefaultLayout>
);

export default HomePage;
