import type { AppProps } from 'next/app';
import { SWRConfig } from 'swr';
import { fetcher } from '@/api/fetcher';
import Header from '@/components/layout/Header';
import Footer from '@/layout/Footer';
import '../node_modules/focus-visible/dist/focus-visible';
import '../styles/globals.css';

function MaadApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Header />
      <SWRConfig value={{ fetcher }}>
        <Component {...pageProps} />
      </SWRConfig>
      <Footer />
    </>
  );
}

export default MaadApp;
