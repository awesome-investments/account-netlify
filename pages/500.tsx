import Container from '@/layout/Container';
import PageWrapper from '@/layout/PageWrapper';

const Page500 = () => (
  <PageWrapper>
    <Container left right className="pt-10 text-center">
      <h1>Unexpected error occured. Try again later.</h1>
    </Container>
  </PageWrapper>
);

export default Page500;
