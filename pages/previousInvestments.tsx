import Link from 'next/link';
import React from 'react';
import Container from '@/layout/Container';
import DefaultLayout from '@/layout/DefaultLayout';
import PreviousInvestments from '@/components/compositions/previousInvestments/PreviousInvestments';

const PreviousInvestmentPage = () => (
  <DefaultLayout title="Investments" h1="Investments">
    <Container left right className="mb-10">
      <Link href="/">
        <a className="underline">Back to homepage</a>
      </Link>
    </Container>
    <Container left right>
      <PreviousInvestments />
    </Container>
  </DefaultLayout>
);

export default PreviousInvestmentPage;
