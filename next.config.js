module.exports = {
  compress: false,
  eslint: {
    // we have eslint configured as pre-commit hook
    // if we would like to eslint be part of build then
    // 1) yarn add --dev eslint-config-next
    // 2) and adjust .estlintrc config
    ignoreDuringBuilds: true,
  },
  async rewrites() {
    return [];
  },
};
